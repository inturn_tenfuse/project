import { useRouter } from 'next/router'
import React, { useState, useEffect } from 'react'
import AlertWrongInput from '../components/AlertWrongInput'
import NullInput from '../components/NullnputMassage'

export default function addRoomPage() {
  const router = useRouter()
  const initialData = {
    id: "",
    firstname: "",
    lastname: "",
    email: "",
    phone: "",
    username: "",
    password: "",
    position: "SuperAdmin",
  }
  const [data, setData] = useState<any>(initialData)
  const [checkChangeFormActivity, setCheckChangeFormActivity] = useState<any>(false)
  const [errorEmailInput, setErrorEmailInput] = useState<any>(null)
  const [userDataFromLocal, setUserDataFromLocal] = useState<any>([])
  const [checkSave, setCheckSave] = useState<any>(false)
  const [showWrongInputModal, setShowWrongInputModal] = useState<any>(false)


  const checkNullData = data.id !== "" && data.firstname !== "" && data.lastname !== "" &&
    data.email !== "" && data.phone !== "" && data.username !== "" &&
    data.password !== "" && data.position !== "" && errorEmailInput == null

  useEffect(() => {
    setCheckChangeFormActivity(false)
  }, [checkChangeFormActivity])

  useEffect(() => {
    if (checkSave == true) {
      localStorage.setItem("userDataAll", JSON.stringify(userDataFromLocal))
      setCheckSave(false)
    }
  }, [checkSave])

  console.log(userDataFromLocal)

  useEffect(() => {
    const dataInLs = localStorage.getItem("userDataAll") ? JSON.parse(localStorage.getItem("userDataAll")!) : null;
    if (dataInLs) {
      setUserDataFromLocal(dataInLs)
    }
    let level = dataInLs.length + 1
    level = level.toString()
    setData({ ...data, id: level })
  }, [])

  const handleBackButton = () => {
    router.push("./salesUserManagement")
  }

  const handleSaveButton = () => {
    setCheckSave(true)
    if (checkNullData) {
      setUserDataFromLocal([...userDataFromLocal, data])
    }else {
      setShowWrongInputModal(true)
    }
  }

  //validate patturn
  function isValidEmail(email) {
    return /\S+@\S+\.\S+/.test(email);
  }
  return (
    <><div className='pt-4 mx-5 md:mx-10'>
      <div className="mb-6">
        <div className='border-2 p-5 w-max lg:w-full shadow-sm border-gray-200 rounded-3xl bg-white'>
          <div>
            <h3 className='text-3xl mb-4 pb-0.5 border-b-2 border-black'>เพิ่มข้อมูลพนักงาน</h3>
          </div>
          <div className='bg-white p-5 rounded-lg'>
            <div className=" mt-4 lg:mt-5 mx-auto py-5">
              <div className="grid grid-cols-4 gap-4 grid-flow-col">
                <div className="grid">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">firstname<span className='text-xs'>(ใส่คำนำหน้า นาย/นางสาว/นาง)</span></label>
                </div>
                <div className="col-start-2 col-span-2">
                  <input type="text" id="hotelname" autoComplete='off' className="bg-slate-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-2/3 md:w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-7"
                    value={data.firstname}
                    onChange={e => {
                      setData({ ...data, firstname: e.target.value })
                    }}
                    required></input>
                </div>
                <div className="col-start-4 col-span-2">
                  {!data.firstname ? <NullInput message='firstname'></NullInput> : <div className="pt-1"></div>}
                </div>

                <div className="grid">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">lastname</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <input type="text" id="province" autoComplete='off' className="bg-slate-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-2/3 md:w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-7"
                    value={data.lastname}
                    onChange={e => {
                      setData({ ...data, lastname: e.target.value })
                    }}
                    required></input>
                </div>
                <div className="col-start-4 col-span-2">
                  {!data.lastname ? <NullInput message='lastname'></NullInput> : <div className="pt-1"></div>}
                </div>
                <div className="grid">
                  <label className="block mb-2 text-base  font-medium text-gray-900 dark:text-white">email</label>
                </div >
                <div className="col-start-2 col-span-2">
                  <input type="email" id="email" autoComplete='off' className="bg-slate-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-2/3 md:w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-7"
                    value={data.email}
                    onChange={e => {
                      setData({ ...data, email: e.target.value })
                      if (!isValidEmail(e.target.value)) {
                        setErrorEmailInput('Email is invalid');
                      } else {
                        setErrorEmailInput(null);
                      }
                    }}
                    required></input>
                </div>
                <div className="col-start-4 col-span-2">
                  {!data.email ? <NullInput message='email'></NullInput> : <></>}
                  {data.email && errorEmailInput != null ? <NullInput message='emailให้ถูกต้อง'></NullInput> : <div className="pt-1"></div>}
                </div>
                <div className="grid">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">phone</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <input type="text" id="district" autoComplete='off' className="bg-slate-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-2/3 md:w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-7"
                    value={data.phone}
                    onChange={e => {
                      setData({ ...data, phone: e.target.value })
                    }}
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    required>
                  </input>
                </div>
                <div className="col-start-4 col-span-2">
                  {!data.phone ? <NullInput message='phone'></NullInput> : <div className="pt-1"></div>}
                </div>
                <div className="grid">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">username</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <input type="tel" id="locality" autoComplete='off' className="bg-slate-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-2/3 md:w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-7"
                    value={data.username}
                    onChange={e => {
                      setData({ ...data, username: e.target.value })
                    }}
                    required></input>
                </div>
                <div className="col-start-4 col-span-2">
                  {!data.username ? <NullInput message='username'></NullInput> : <div className="pt-1"></div>}
                </div>
                <div className="grid">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">password</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <input type="tel" id="locality" autoComplete='off' className="bg-slate-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-2/3 md:w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-7"
                    value={data.password}
                    onChange={e => {
                      setData({ ...data, password: e.target.value })
                    }}
                    required></input>
                </div>
                <div className="col-start-4 col-span-2">
                  {!data.password ? <NullInput message='password'></NullInput> : <div className="pt-1"></div>}
                </div>

                <div className="grid">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">position</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <select name="status" placeholder="Choose a status"
                    className="bg-slate-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-32 p-1 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-7"
                    value={data.position}
                    onChange={e => {
                      setData({ ...data, position: e.target.value })
                    }}>
                    <option value="Superadmin">SuperAdmin</option>
                    <option value="Admin">Admin</option>
                    <option value="Employee">Employee</option>
                  </select>
                </div>
              </div>
            </div >
          </div>
        </div>
      </div>
    </div>
      <div className='px-10 py-5 flex justify-end'>
        <button type="submit" onClick={handleSaveButton} className="text-white mx-3 bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-auto sm:w-24 px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">บันทึก</button>
        <button onClick={handleBackButton} className="text-white mx-3 bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-auto sm:w-24 px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">กลับ</button>
      </div>
      {showWrongInputModal == true ? <AlertWrongInput setShowWrongInputModal={setShowWrongInputModal}></AlertWrongInput> : <></>}
</>
  )
}