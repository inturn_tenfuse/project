import { useRouter } from 'next/router'
import { getCurrentRoomDetail } from '../components/GetData'
import React, { useState } from 'react'
export default function viewRoomPage() {

  const router = useRouter()
  const [currentDataView] = useState<any>(getCurrentRoomDetail())

  const handleBackButton = () => {
    router.push("./roomManagement")
  }


  return (
    <>
      <div className='pt-4 mx-5 md:mx-10'>
        <div className="mb-6">
          <div className='border-2 p-5 w-max xl:w-full shadow-sm border-gray-200 rounded-3xl bg-white'>
            <div>
              <h3 className='text-3xl mb-10 pb-0.5 border-b-2 border-black'>ข้อมูลที่พัก</h3>
            </div>
            <div className='bg-white p-5 rounded-lg'>
              <div className="grid grid-cols-4 gap-4 grid-flow-col">
                <div className="grid">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">ชื่อที่พัก:</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">{currentDataView.ชื่อที่พัก}</label>
                </div>
                <div className="grid">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">จังหวัด:</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">{currentDataView.จังหวัด}</label>
                </div>
                <div className="grid">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">อำเภอ:</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">{currentDataView.อำเภอ}</label>
                </div>
                <div className="grid">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">ตำบล:</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">{currentDataView.ตำบล}</label>
                </div>
                <div className="grid">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">รูปแบบการติดต่อ:</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">{currentDataView.รูปแบบการติดต่อ}</label>
                </div>
                <div className="grid">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">สถานะการจอง:</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">{currentDataView.สถานะการจอง}</label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='px-10 py-5 flex justify-end'>
        <button onClick={handleBackButton} className="text-white mx-3 bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-auto sm:w-24 px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">กลับ</button>
      </div>
    </>
  )
}