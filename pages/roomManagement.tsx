import React, { useState, useEffect } from 'react'
import TableRoom from '../components/TableRoom'

import * as xlsx from 'xlsx';
import { useRouter } from 'next/router'
import ExcelExport from '../components/Excelexport';


export const Case = ({ children }) => children;

export default function roomManagement() {
  const router = useRouter()
  const [excelDataFromLocal, setExcelDataFromLocal] = useState<any>([])
  const [checkChangeData, setCheckChangeData] = useState<any>(false);

  const columns = [
    { label: "ชื่อที่พัก", accessor: "ชื่อที่พัก", sortable: true, currentOrder: "" },
    { label: "จังหวัด", accessor: "จังหวัด", sortable: false, currentOrder: "" },
    { label: "อำเภอ", accessor: "อำเภอ", sortable: true, currentOrder: "", sortbyOrder: "desc" },
    { label: "ชื่อผู้ติดต่อ", accessor: "ชื่อผู้ติดต่อ", sortable: true, currentOrder: "" },
    { label: "รูปแบบการติดต่อ", accessor: "รูปแบบการติดต่อ", sortable: true, currentOrder: "" },
    { label: "สถานะการจอง", accessor: "สถานะการจอง", sortable: true, currentOrder: "" },
    { label: "action", accessor: "action", sortable: true, currentOrder: "" }
  ];

  const initialSearch = ""
  const [excelFileError, setExcelFileError] = useState<any>("please select your excel file");
  const [excelData, setExcelData] = useState<any>([]);
  const [filteredData, setFilteredData] = useState<any>(initialSearch);
  //create for check data in local if data have a change => pull data in local again
  const [changeDataInLocal, setChangeDataInLocal] = useState<any>(false)
  const [checkCurrentPage, setCheckCurrentPage] = useState<any>(false)

  const handleAddLead = () => {
    router.push("./addRoomPage")
  }

  useEffect(() => {
  }, [filteredData])

  console.log(excelDataFromLocal)

  useEffect(() => {
    getAllDataRoomDetailFromLocal()
  }, [])

  useEffect(() => {
    getAllDataRoomDetailFromLocal()
    if (excelDataFromLocal) {
      setFilteredData(excelDataFromLocal)
    }
  }, [excelDataFromLocal]);

  useEffect(() => {
    if (changeDataInLocal == true) {
      getAllDataRoomDetailFromLocal()
      setChangeDataInLocal(false)
    }
  }, [changeDataInLocal]);


  useEffect(() => {
    const data = localStorage.getItem("roomDetail") ? JSON.parse(localStorage.getItem("roomDetail")!) : null;
    if (data) {
      setExcelDataFromLocal(data);
    }
    setCheckChangeData(false)
  }, [checkChangeData]);



  //file
  const readExcel = async (e) => {
    const file = e?.target.files[0];
    if (file) {
      setExcelFileError("")
      const data = await file.arrayBuffer(file);
      const excelfile = xlsx.read(data, {
        cellText: false, cellDates: true
      });
      const excelsheet = excelfile.Sheets[excelfile.SheetNames[0]];
      const exceljson = xlsx.utils.sheet_to_json(excelsheet, { raw: false, dateNF: 'yyyy-mm-dd' });
      // console.log(data);
      // console.log(excelfile);
      // console.log(excelsheet);
      // console.log(exceljson);
      setExcelData(exceljson);
    } else {
      setExcelFileError("please select your excel file")
    }
  }

  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    result = excelDataFromLocal.filter((data) => {
      return data.ชื่อที่พัก.search(value) != -1;
    });
    setFilteredData(result);
  }

  const clickReadExcel = () => {
    setCheckChangeData(true)
    if (excelData != null) {
      localStorage.setItem('roomDetail', JSON.stringify(excelData));
      setCheckCurrentPage(true)
      setChangeDataInLocal(true)
    } if (excelData.length == 0) {
      localStorage.setItem('showDataRoom', JSON.stringify([]))
    }
  }

  useEffect(() => {
    const data = localStorage.getItem("roomDetail") ? JSON.parse(localStorage.getItem("roomDetail")!) : null;
    if (data) {
      setExcelDataFromLocal(data);
    }
    setCheckChangeData(false)
  }, [checkChangeData]);

  useEffect(() => {
  }, [excelDataFromLocal]);

  return (
    <>
      <h1 className='text-lg font-extrabold text-left text-gray-400 pt-6  px-5'>จัดการที่พัก</h1>
      <div className=" p-8 rounded-md w-full">
        <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
          <div className='py-3'></div>
          <div className='flex justify-between'>
            <div className="justify-start">
              <span className="sr-only ">เลือกไฟล์</span>
              <input type="file" className=" w-64 text-sm text-gray-500 bg-slate-200 file:mr-4 file:h-10 file:py-2 file:px-4 file:w-28 rounded-lg file:border-0 file:text-sm file:font-semibold file:bg-slate-300 file:text-black hover:file:bg-slate-400"
                accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                onChange={readExcel} required />
            </div>
            <div className="flex">
              <label className="sr-only">ค้นหา</label>
              <div className=" relative w-48">
                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                  <svg aria-hidden="true" className="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path></svg>
                </div>
                <input type="text" onChange={(event) => handleSearch(event)} id="simple-search" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="ค้นหาชื่อที่พัก" required />
              </div>
              <div className='pl-5'>
                <button onClick={handleAddLead} className="bg-blue-600 hover:bg-blue-700 px-4 py-2 w-auto sm:w-32 rounded-md text-white font-semibold tracking-wide cursor-pointer">เพิ่มข้อมูล</button>
              </div>
              <div className='px-2 '>
                <ExcelExport excelData={excelDataFromLocal} fileName={"Myexcel"} />
                {/* <button title='ดาวน์โหลดข้อมูลเป็นไฟล์ Excel' className=' p-2 text-md bg-blue-600 hover:bg-blue-700 w-auto rounded-md text-white'
                ><MdFileDownload size={22} ></MdFileDownload></button> */}
              </div>
            </div>

          </div>

          {excelFileError && !excelDataFromLocal ? <div className='text-rose-600 ml-28'>{excelFileError}</div> : <div className='mt-3'></div>}

          <div className='pb-5 pt-1'>
            <button title='ดึงข้อมูลจากไฟล์เอกเซลล์ที่เลือก' className='text-md bg-blue-600 hover:bg-blue-700 py-1 w-24 md:w-28 rounded-md text-white font-semibold tracking-wide cursor-pointer'
              onClick={clickReadExcel}> ยืนยัน </button>
          </div>
          <div className="table_container">
            <TableRoom
              data={filteredData}
              columns={columns}
              changeDataInLocal={changeDataInLocal}
              setChangeDataInLocal={setChangeDataInLocal}
            />
          </div>
        </div>
      </div>
    </>
  )
  function getAllDataRoomDetailFromLocal() {
    const data = localStorage.getItem("roomDetail") ? JSON.parse(localStorage.getItem("roomDetail")!) : null
    if (data) {
      setFilteredData(data)
    }
  }
}