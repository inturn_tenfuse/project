import { useRouter } from 'next/router'
import { getCurrentRoomDetail } from '../components/GetData'
import React, { useEffect, useState } from 'react'
export default function viewRoomPage() {

  const router = useRouter()
  const [currentUserDataView, setCurrentUserDataView] = useState<any>({})

  useEffect(() => {
    const data = localStorage.getItem("currentUserData") ? JSON.parse(localStorage.getItem("currentUserData")!) : null;
    if (data) {
      setCurrentUserDataView(data)
    }
  },[])

  console.log(currentUserDataView)
  

  const handleBackButton = () => {
    router.push("./salesUserManagement")
  }


  return (
    <>
      <div className='pt-4 mx-5 md:mx-10'>
        <div className="mb-6">
          <div className='border-2 p-5 w-max lg:w-full shadow-sm border-gray-200 rounded-3xl bg-white'>
            <div>
              <h3 className='text-3xl mb-4 pb-0.5 border-b-2 border-black'>ข้อมูลบัญชีผู้ใช้</h3>
            </div>
            <div className='bg-white p-5 rounded-lg mt-5'>
              <div className="grid grid-cols-4 gap-4 grid-flow-col">
                <div className="grid">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">ชื่อ-นามสกุล:</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">{currentUserDataView.firstname} {currentUserDataView.lastname}</label>
                </div>
                <div className="grid">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">อีเมล์:</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">{currentUserDataView.email}</label>
                </div>
                <div className="grid">
                  <label className="block mb-2 text-base  font-medium text-gray-900 dark:text-white">เบอร์โทรศัพท์:</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">{currentUserDataView.phone}</label>
                </div>
                <div className="grid">
                  <label className="block mb-2 text-base  font-medium text-gray-900 dark:text-white">ชื่อผู้ใช้:</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">{currentUserDataView.username}</label>
                </div>
                <div className="grid">
                  <label className="block mb-2 text-base  font-medium text-gray-900 dark:text-white">รหัสผ่าน:</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">{currentUserDataView.password}</label>
                </div>
                <div className="grid">
                  <label className="block mb-2 text-base  font-medium text-gray-900 dark:text-white">ตำแหน่ง:</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">{currentUserDataView.position}</label>
                </div>
                <div className="grid">
                  <label className="block mb-2 text-base  font-medium text-gray-900 dark:text-white">จำนวนงานที่เสร็จ:</label>
                </div>
                <div className="col-start-2 col-span-2">
                  <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">{currentUserDataView.complete_job} รายการ</label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='px-10 py-5 flex justify-end'>
        <button onClick={handleBackButton} className="text-white mx-3 bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-auto sm:w-24 px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">กลับ</button>
      </div>
    </>
  )
}