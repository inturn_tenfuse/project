import '../styles/globals.css'
import type { AppProps } from 'next/app'
import SideNav from '../components/SideNav';
import { useRouter } from 'next/router';

export default function App({ Component, pageProps }: AppProps) {
  const router = useRouter();
  if (router.pathname === '/') {
    return (
      <>
        <Component {...pageProps} />
      </>
    )
  }
  return (
    <>
      <div className='bg-gray-100'>
        <SideNav></SideNav>
        <div className='ml-56 h-screen overflow-y-auto overflow-x-hidden'>
          <Component {...pageProps} />
        </div>
      </div>
    </>
  )

}
