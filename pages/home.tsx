import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts';
import { graph1 } from '../data/graph/graph1'
import { graph2 } from '../data/graph/graph2'



export default function home() {

  //change mount to full form
  const getIntroOfMount = (label) => {
    switch (label) {
      case 'ม.ค.': return "มกราคม"
      case 'ก.พ.': return "กุมภาพันธ์"
      case 'มี.ค.': return "มีนาคม"
      case 'เม.ย.': return "เมษายน"
      case 'พ.ค.': return "พฤษภาคม"
      case 'มิ.ย.': return "มิถุนายน"
      case 'ก.ค.': return "กรกฎาคม"
      case 'ส.ค.': return "สิงหาคม"
      case 'ก.ย.': return "กันยายน"
      case 'ต.ค.': return "ตุลาคม"
      case 'พ.ย.': return "พฤศจิกายน"
      case 'ธ.ค.': return "ธันวาคม"
      default: ""
    }

  };

  const CustomTooltip = ({ payload }) => {
    if (payload && payload.length) {
      return (
        <div className="p-2 bg-white text-xs">
          <p className="label">{`เดือน${getIntroOfMount(payload[0].payload.เดือน)}`}</p>
          <p className="desc">มีรายการจองห้องทั้งหมด {payload[0].value} รายการ</p>
        </div>
      );
    }

    return null;
  };

  return (
    <>
      <section className="flex flex-col w-full antialiased text-gray-600 min-h-screen ">
        <h2 className='text-lg font-extrabold text-left text-gray-400 py-6 px-5'>หน้าหลัก</h2>
        <div className="lg:max-w-full max-w-xl p-5 sm:pl-8 sm:pt-3 h-full ">
          <div className="flex flex-col col-span-full xl:col-span-8 bg-white shadow-lg border border-gray-200 rounded-3xl">
            <header className="px-5 py-4 border-b border-gray-100 flex items-center">
              <h2 className="font-semibold text-gray-800">สถานะของที่พักที่ได้รับการติดต่อ</h2>
            </header>
            <div className="px-5 py-1">
              <div className="flex flex-wrap">
                <div className="flex items-center py-2">
                  <div className="mr-8 ml-8 my-4 md:ml-16 md:mr-16">
                    <div className="flex items-center">
                      <div className="text-3xl font-bold text-gray-800 mr-2">24</div>
                      <div className="text-sm font-medium text-green-500">+49%</div>
                    </div>
                    <div className="text-sm text-gray-500">รายการที่คอนเฟิรม์แล้ว</div>
                  </div>
                  <div className="hidden md:block w-px h-8 bg-gray-200 mr-5" aria-hidden="true"></div>
                </div>
                <div className="flex items-center py-2">
                  <div className="mr-8 ml-8 my-4 md:ml-16 md:mr-16">
                    <div className="flex items-center">
                      <div className="text-3xl font-bold text-gray-800 mr-2">56</div>
                      <div className="text-sm font-medium text-green-500">+7%</div>
                    </div>
                    <div className="text-sm text-gray-500">รายการที่กำลังดำเนินการ</div>
                  </div>
                  <div className="hidden md:block w-px h-8 bg-gray-200 mr-5" aria-hidden="true"></div>
                </div>
                <div className="flex items-center py-2">
                  <div className="mr-8 ml-8 my-4 md:ml-16 md:mr-16">
                    <div className="flex items-center">
                      <div className="text-3xl font-bold text-gray-800 mr-2">54</div>
                      <div className="text-sm font-medium text-yellow-500">-7%</div>
                    </div>
                    <div className="text-sm text-gray-500">รายการที่รอข้อมูลเพิ่มเติม </div>
                  </div>

                  <div className="hidden md:block w-px h-8 bg-gray-200 mr-5" aria-hidden="true"></div>
                </div>
                <div className="flex items-center">
                  <div className="mr-8 ml-8 my-4 md:ml-16 md:mr-16">
                    <div className="flex items-center">
                      <div className="text-3xl font-bold text-gray-800 mr-2">6</div>
                      <div className="text-sm font-medium text-yellow-500">+7%</div>
                    </div>
                    <div className="text-sm text-gray-500">รายการที่ปฏิเสธ</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="flex-grow">
              {/* <canvas id="analytics-card-01" width="800" height="300">
              </canvas> */}
            </div>
          </div>
        </div>
        {/* graph 1 */}
        <div className="flex">
          <div className="w-full xl:w-1/2 mb-12 xl:mb-0 px-8 ">
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-3xl">
              <div className="rounded-t mb-0 px-4 py-3 border-0">
                <div className="flex flex-wrap items-center">
                  <div className="relative w-full px-4 max-w-full flex-grow flex-1">
                    <h3 className="font-semibold text-gray-800">อันดับของพนักงาน</h3>
                  </div>
                </div>
              </div>
              <div className="block w-full overflow-x-auto">
                <table className="items-center bg-transparent w-full border-collapse ">
                  <thead>
                    <tr>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-sm uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        อันดับ
                      </th>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-sm uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        ชื่อ
                      </th>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-sm uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        ยอดงานที่สำเร็จ
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th className="border-t-0 px-6 border-l-0 border-r-0 text-xs whitespace-nowrap text-gray-500 p-4 text-left ">
                        1
                      </th>
                      <td className="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap text-gray-500 p-4">
                        นายอิทธิพล ชิณวังโส
                      </td>
                      <td className="border-t-0 px-6 align-center border-l-0 border-r-0 text-sm whitespace-nowrap p-4">
                        25
                      </td>
                    </tr>
                    <tr>
                      <th className="border-t-0 px-6 border-l-0 border-r-0 text-xs whitespace-nowrap text-gray-500 p-4 text-left">
                        2
                      </th>
                      <td className="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap text-gray-500 p-4">
                        นางจาริยา รัชตาธิวัฒน์
                      </td>
                      <td className="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap text-gray-500 p-4">
                        20
                      </td>
                    </tr>
                    <tr>
                      <th className="border-t-0 px-6 border-l-0 border-r-0 text-xs whitespace-nowrap text-gray-500 p-4 text-left">
                        3
                      </th>
                      <td className="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap text-gray-500 p-4">
                        นายพงศกร กลิ่นหอม
                      </td>
                      <td className="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap text-gray-500 p-4">
                        12
                      </td>
                    </tr>
                    <tr>
                      <th className="border-t-0 px-6 border-l-0 border-r-0 text-xs whitespace-nowrap text-gray-500 p-4 text-left">
                        4
                      </th>
                      <td className="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap text-gray-500 p-4">
                        นายพชร ว่องไว
                      </td>
                      <td className="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap text-gray-500 p-4">
                        9
                      </td>
                    </tr>
                    <tr>
                      <th className="border-t-0 px-6 border-l-0 border-r-0 text-xs whitespace-nowrap text-gray-500 p-4 text-left">
                        5
                      </th>
                      <td className="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap text-gray-500 p-4">
                        นายสิทธิโชค วงศ์ตระกูล
                      </td>
                      <td className="border-t-0 px-6 border-l-0 border-r-0 text-sm whitespace-nowrap text-gray-500 p-4">
                        5
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className="md:w-1/2 max-w-2xl sm:pr-6 h-full ">
            <div className="flex flex-col col-span-full xl:col-span-8 bg-white shadow-lg border border-gray-200 rounded-3xl">
              <header className="px-5 py-4 border-b border-gray-100 flex items-center">
                <h2 className="font-semibold text-gray-800">ภาพรวมการจองผ่านระบบ</h2>
              </header>
              <div className="px-5 py-2">
                <div className="flex-grow mt-10">
                  <LineChart width={550} height={200} data={graph2} margin={{ top: 12, right: 20, bottom: 5, left: 0 }}>
                    <Line type="monotone" dataKey="จำนวนจองห้องพักทั้งหมด" stroke="#8884d8" />
                    <CartesianGrid stroke="#fff" strokeDasharray="2 2" />
                    <XAxis dataKey="เดือน" />
                    <YAxis />
                    <Tooltip content={<CustomTooltip payload={graph2} />} />
                  </LineChart>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}
