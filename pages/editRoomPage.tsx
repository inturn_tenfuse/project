import { useRouter } from 'next/router'
import React, { useState, useEffect } from 'react'
import { getAllRoomDetail, getCurrentRoomDetail } from '../components/GetData'
import NullInput from '../components/NullnputMassage'
import { userData } from '../data/usersData'
import AlertWrongInput from '../components/AlertWrongInput'



export default function editRoomPage() {
  const router = useRouter()
  const [data, setData] = useState<any>(getCurrentRoomDetail())
  const [formActivity, setFormActivity] = useState<any>([])
  const [checkChangeFormActivity, setCheckChangeFormActivity] = useState<any>(false)
  const [excelDataFromLocal, setExcelDataFromLocal] = useState<any>(getAllRoomDetail())
  const [showWrongInputModal, setShowWrongInputModal] = useState<any>(false)


  const checkNullData = data.ลำดับ !== "" && data.ชื่อที่พัก !== "" && data.จังหวัด !== "" &&
    data.อำเภอ !== "" && data.ตำบล !== "" && data.ชื่อผู้ติดต่อ !== "" &&
    data.เบอร์โทรผู้ติดต่อ !== "" && data.อีเมล์ !== "" && data.กิจกรรม !== "" &&
    data.รายละเอียดเพิ่มเติม !== ""

  //set value in formActiviti and put it in data
  const clickCheckboxActivity = (e) => {
    if (e.target.checked == true) {
      setFormActivity([...formActivity, e.target.value])
      setCheckChangeFormActivity(true)
    }
    else {
      setFormActivity(formActivity.filter(item => item !== e.target.value))
      setCheckChangeFormActivity(true)

    }
  }

  useEffect(() => {
    if (data)
      setData({ ...data, กิจกรรม: "-" })
  }, [])

  useEffect(() => {
    setActicityInExcelDataFormLocal()
    setCheckChangeFormActivity(false)
  }, [checkChangeFormActivity])

  useEffect(() => {
    localStorage.setItem("roomDetail", JSON.stringify(excelDataFromLocal))
  }, [excelDataFromLocal])

  const handleBackButton = () => {
    router.push("./roomManagement")
  }

  console.log(data)

  const saveDataUser = (event) => {
    const object = data
    userData.map(item => {
      if (item.id == event) {
        data.เบอร์โทรผู้ติดต่อ = item.phone
        data.อีเมล์ = item.email
        setData(object)
      }
    })
    console.log(data)
  }


  // change data form array to string if have activity 
  function setActicityInExcelDataFormLocal() {
    if (formActivity.length > 0) {
      const newFormActivityArray = formActivity.join(",")
      setData({ ...data, กิจกรรม: newFormActivityArray })
    } else {
      setData({ ...data, กิจกรรม: "-" })
    }
  }

  const handleSaveButton = () => {
    // setExcelDataFromLocal(getAllRoomDetail())
    setBlankDetailInData()
    setBlankActivityData()
    if (checkNullData) {
      console.log("yes")
      const arr = [...excelDataFromLocal]
      const editInIndex = data.ลำดับ - 1
      arr[editInIndex] = data
      setExcelDataFromLocal(arr)
    } else {
      setShowWrongInputModal(true)
    }
  }



  return (
    <>
      <div className='pt-4 mx-5 md:mx-10'>
        <div className="mb-6">
          <div className='border-2 p-5 w-max xl:w-full shadow-sm border-gray-200 rounded-3xl bg-white'>
            <div>
              <h3 className='text-3xl mb-4 pb-0.5 border-b-2 border-black'>แก้ไขเพิ่มข้อมูล</h3>
            </div>
            <div className='bg-white p-5 rounded-lg'>
              <div className=" mt-4 mx-auto py-5">
                <div className="grid grid-cols-4 gap-4 grid-flow-col">
                  <div className="grid">
                    <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">ชื่อที่พัก</label>
                  </div>
                  <div className="col-start-2 col-span-2">
                    <input type="text" id="hotelname" autoComplete='off' className="bg-slate-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-2/3 md:w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-7"
                      value={data.ชื่อที่พัก}
                      onChange={e => {
                        setData({ ...data, ชื่อที่พัก: e.target.value })
                      }}
                      required></input>
                  </div>
                  <div className="col-start-4 col-span-2">
                    {!data.ชื่อที่พัก ? <NullInput message='ชื่อ'></NullInput> : <div className="pt-1"></div>}
                  </div>

                  <div className="grid">
                    <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">จังหวัด</label>
                  </div>
                  <div className="col-start-2 col-span-2">
                    <input type="text" id="province" autoComplete='off' className="bg-slate-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-2/3 md:w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-7"
                      value={data.จังหวัด}
                      onChange={e => {
                        setData({ ...data, จังหวัด: e.target.value })
                      }}
                      required></input>
                  </div>
                  <div className="col-start-4 col-span-2">
                    {!data.จังหวัด ? <NullInput message='จังหวัด'></NullInput> : <div className="pt-1"></div>}
                  </div>
                  <div className="grid">
                    <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">อำเภอ</label>
                  </div>
                  <div className="col-start-2 col-span-2">
                    <input type="text" id="district" autoComplete='off' className="bg-slate-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-2/3 md:w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-7"
                      value={data.อำเภอ}
                      onChange={e => {
                        setData({ ...data, อำเภอ: e.target.value })
                      }}
                      required>
                    </input>
                  </div>
                  <div className="col-start-4 col-span-2">
                    {!data.อำเภอ ? <NullInput message='อำเภอ'></NullInput> : <div className="pt-1"></div>}
                  </div>
                  <div className="grid">
                    <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">ตำบล</label>
                  </div>
                  <div className="col-start-2 col-span-2">
                    <input type="tel" id="locality" autoComplete='off' className="bg-slate-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-2/3 md:w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-7"
                      value={data.ตำบล}
                      onChange={e => {
                        setData({ ...data, ตำบล: e.target.value })
                      }}
                      pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                      required></input>
                  </div>
                  <div className="col-start-4 col-span-2">
                    {!data.ตำบล ? <NullInput message='ตำบล'></NullInput> : <div className="pt-1"></div>}
                  </div>
                  <div className="grid">
                    <label className="block mb-2 text-base  font-medium text-gray-900 dark:text-white">กิจกรรม</label>
                  </div>
                  <div className="col-start-2 col-span-2">
                    <div className='flex pt-2'>
                      <div className="flex items-center mr-4">
                        <input onClick={clickCheckboxActivity} name="กิจกรรม" type="checkbox" value="โทรศัพท์" className="w-4 h-4 rounded dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
                        <label className="ml-2 text-sm font-medium dark:text-gray-300">โทรศัพท์</label>
                      </div>
                      <div className="flex items-center mr-4">
                        <input onClick={clickCheckboxActivity} name="กิจกรรม" type="checkbox" value="อีเมล์" className="w-4 h-4 rounded dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
                        <label className="ml-2 text-sm font-medium dark:text-gray-300">อีเมล์</label>
                      </div>
                      <div className="flex items-center mr-4">
                        <input onClick={clickCheckboxActivity} name="กิจกรรม" type="checkbox" value="เว็ปไซต์" className="w-4 h-4 rounded dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
                        <label className="ml-2 text-sm font-medium dark:text-gray-300">เว็ปไซต์</label>
                      </div>
                      <div className="flex items-center mr-4">
                        <input onClick={clickCheckboxActivity} name="กิจกรรม" type="checkbox" value="โรงแรม" className="w-4 h-4 rounded dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
                        <label className="ml-2 text-sm font-medium dark:text-gray-300">โรงแรม</label>
                      </div>
                    </div>
                  </div>

                  <div className="grid">
                    <label className="block mb-2 text-base  font-medium text-gray-900 dark:text-white">รายละเอียด</label>
                  </div>
                  <div className="col-start-2 col-span-2">
                    <textarea id="detail_reserve" maxLength={1000} className="h-20 block resize-none mb-2 p-1 w-2/3 md:w-full text-sm text-gray-900 bg-slate-50 rounded-sm border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      value={data.รายละเอียดเพิ่มเติม}
                      onChange={e => {
                        setData({ ...data, รายละเอียดเพิ่มเติม: e.target.value })
                      }}>
                    </textarea>
                  </div>
                  <div className="grid">
                    <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">สถานะการจอง</label>
                  </div>
                  <div className="col-start-2 col-span-2">
                    <select name="name_Seller"
                      className="bg-slate-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-auto p-1 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-7"
                      onChange={e => { saveDataUser(e.target.value) }}>
                      {userData.map(item =>
                        <option value={item.id}>{item.firstname} {item.lastname}</option>
                      )}
                    </select>
                  </div>
                  <div className="grid">
                    <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">รูปแบบการติดต่อ</label>
                  </div>
                  <div className="col-start-2 col-span-2">
                    <select name="contact_type" placeholder="Choose a contact"
                      className="bg-slate-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-32 p-1 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-7"
                      value={data.การติดต่อ}
                      onChange={e => {
                        setData({ ...data, รูปแบบการติดต่อ: e.target.value })
                      }}>
                      <option defaultValue={"Walk-in"}>Walk-in</option>
                      <option value="Online">Online</option>
                    </select>
                  </div>

                  <div className="grid">
                    <label className="block mb-2 text-base font-medium text-gray-900 dark:text-white">สถานะการจอง</label>
                  </div>
                  <div className="col-start-2 col-span-2">
                    <select name="status" placeholder="Choose a status"
                      className="bg-slate-50 border border-gray-300 text-gray-900 text-sm rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-32 p-1 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-7"
                      value={data.สถานะ}
                      onChange={e => {
                        setData({ ...data, สถานะการจอง: e.target.value })
                      }}>
                      <option defaultValue={"Not-Started"}>Not-Started</option>
                      <option value="Reject">Reject</option>
                      <option value="Waiting Info">Waiting Info</option>
                      <option value="In Process">In Process</option>
                      <option value="Confirm">Confirm</option>
                    </select>
                  </div>
                </div>
              </div >
            </div>
          </div>
        </div>
      </div>
      <div className='px-10 py-5 flex justify-end'>
        <button type="submit" onClick={handleSaveButton} className="text-white mx-3 bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-auto sm:w-24 px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">บันทึก</button>
        <button onClick={handleBackButton} className="text-white mx-3 bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-auto sm:w-24 px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">กลับ</button>
      </div>
      {showWrongInputModal == true ? <AlertWrongInput setShowWrongInputModal={setShowWrongInputModal}></AlertWrongInput> : <></>}
    </>
  )

  function setBlankActivityData() {
    if (data.กิจกรรม == "") {
      setData({ ...data, กิจกรรม: "-" })
    }
  }

  function setBlankDetailInData() {
    if (data.รายละเอียดเพิ่มเติม == "") {
      setData({ ...data, รายละเอียดเพิ่มเติม: "-" })
    }
  }
}

