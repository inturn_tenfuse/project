import React, { useState, useEffect } from 'react'
import TableSalseUser from '../components/TableSalseUser'
import { useRouter } from 'next/router'

export const Case = ({ children }) => children;

export default function roomManagement() {
  const router = useRouter()
  const [userDataAll, setUserDataAll] = useState<any>([])

  const [excelDataFromLocal, setExcelDataFromLocal] = useState<any>([])
  const [checkChangeData, setCheckChangeData] = useState<any>(false);

  const columns = [
    { label: "ลำดับ", accessor: "id", sortable: true, currentOrder: "" },
    { label: "ชื่อ", accessor: "firstname", sortable: false, currentOrder: "" },
    { label: "นามสกุล", accessor: "lastname", sortable: false, currentOrder: "" },
    { label: "อีเมลล์", accessor: "email", sortable: true, currentOrder: "", sortbyOrder: "desc" },
    { label: "เบอร์โทรศัพท์", accessor: "phone", sortable: true, currentOrder: "" },
    { label: "ตำแหน่ง", accessor: "position", sortable: true, currentOrder: "" },
    { label: "action", accessor: "action", sortable: true, currentOrder: "" }
  ];

  const initialSearch = ""
  const [excelFileError, setExcelFileError] = useState<any>("please select your excel file");
  const [filteredData, setFilteredData] = useState<any>(initialSearch);
  //create for check data in local if data have a change => pull data in local again
  const [changeDataInLocal, setChangeDataInLocal] = useState<any>(false)

  const handleAddUser = () => {
    router.push("./addSalesPage")
  }

  useEffect(() => {
  }, [filteredData])

  useEffect(() => {
    const data = localStorage.getItem("userDataAll") ? JSON.parse(localStorage.getItem("userDataAll")!) : null
    if (data) {
      setExcelDataFromLocal(data)
    }
    getAllUserDataAllFromLocal()
  }, [])

  useEffect(() => {
    getAllUserDataAllFromLocal()
    if (userDataAll) {
      setFilteredData(userDataAll)
    }
  }, [userDataAll]);

  useEffect(() => {
    if (changeDataInLocal == true) {
      getAllUserDataAllFromLocal()
      setChangeDataInLocal(false)
    }
  }, [changeDataInLocal]);


  useEffect(() => {
    const data = localStorage.getItem("userDataAll") ? JSON.parse(localStorage.getItem("userDataAll")!) : null;
    if (data) {
      setUserDataAll(data);
    }
    setCheckChangeData(false)
  }, [checkChangeData]);

  console.log(excelDataFromLocal)

  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    result = excelDataFromLocal.filter((data) => {
      const fullname = data.firstname + " " + data.lastname
      if (value != " ")
        return fullname.search(value) != -1;
    });
    setFilteredData(result);
  }

  useEffect(() => {
    const data = localStorage.getItem("userDataAll") ? JSON.parse(localStorage.getItem("userDataAll")!) : null;
    console.log(data)
    if (data) {
      setUserDataAll(data);
    }
    setCheckChangeData(false)
  }, [checkChangeData]);

  useEffect(() => {
  }, [userDataAll]);

  return (
    <>
      <h1 className='text-4lg font-extrabold text-left text-gray-400 pt-6  px-5'>จัดการที่พัก</h1>
      <div className=" p-8 rounded-md w-full">
        <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
          <div className='py-3'></div>
          <div className='flex justify-end'>
            <div className="flex">
              <label className="sr-only">ค้นหา</label>
              <div className=" relative w-48">
                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                  <svg aria-hidden="true" className="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path></svg>
                </div>
                <input type="text" onChange={(event) => handleSearch(event)} id="simple-search" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="ค้นหาชื่อพนักงาน" required />
              </div>
              <div className='pl-5'>
                <button onClick={handleAddUser} className="bg-blue-600 hover:bg-blue-700 px-4 py-2 w-auto sm:w-32 rounded-md text-white font-semibold tracking-wide cursor-pointer">เพิ่มผู้ใช้</button>
              </div>
            </div>

          </div>

          {excelFileError && !excelDataFromLocal ? <div className='text-rose-600 ml-28'>{excelFileError}</div> : <div className='mt-3'></div>}
          <div className="table_container">
            <TableSalseUser
              data={filteredData}
              columns={columns}
              changeDataInLocal={changeDataInLocal}
              setChangeDataInLocal={setChangeDataInLocal}
            />
          </div>
        </div>
      </div>
    </>
  )

  function getAllUserDataAllFromLocal() {
    const data = localStorage.getItem("userDataAll") ? JSON.parse(localStorage.getItem("userDataAll")!) : null
    if (data) {
      setFilteredData(data)
    }
  }
}