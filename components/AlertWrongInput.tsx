import React from 'react'

export default function AlertWrongInput({setShowWrongInputModal}) {
  return (
    <><div
      className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
      <div className="relative w-auto mt-6 mb-2 mx-auto max-w-sm">
        {/*content*/}
        <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
          {/*body*/}
          <div className="relative p-6 flex-auto">
            <p className="my-3 text-slate-500 text-lg leading-relaxed">
              ข้อมูลไม่ถูกต้อง
            </p>
            <p className="my-3 text-slate-500 text-lg leading-relaxed">
              กรุณาใส่รหัสผ่านอีกครั้ง!!
            </p>
          </div>
          {/*footer*/}
          <div className="flex items-center justify-end p-5 border-solid border-slate-200 rounded-b">
            <button
              className="bg-rose-600 text-white active:bg-rose-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
              type="button"
              onClick={() => {setShowWrongInputModal(false)}}
            >ปิด</button>
          </div>
        </div>
      </div>
    </div>
      <div className="opacity-25 fixed inset-0 z-40 bg-black"></div></>
  )
}