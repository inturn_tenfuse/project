export function getAllRoomDetail() {
  const data = localStorage.getItem("roomDetail") ? JSON.parse(localStorage.getItem("roomDetail")!) : null;
    if (data) {
      return data
    }

}

export function getCurrentRoomDetail() {
  const data = localStorage.getItem("currentRoomDetail") ? JSON.parse(localStorage.getItem("currentRoomDetail")!) : null;
    if (data) {
      return data
    }
}

export function getCurrentUserData() {
  const data = localStorage.getItem("currentUserData") ? JSON.parse(localStorage.getItem("currentUserData")!) : null;
    if (data) {
      return data
    }
}

export function getUserDataAll() {
  const dataAll = localStorage.getItem("userDataAll") ? JSON.parse(localStorage.getItem("userDataAll")!) : null;
    if (dataAll) {
      return dataAll
    }
}
