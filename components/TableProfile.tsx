
export default function Table({ user }) {

    return (
        <>

            <div className="flex">

                {/* picture */}
                <div className=" lg:w-max ml-2 mt-12 mr-8 rounded-lg overflow-hidden shadow-lg w-96">
                    <div className="flex justify-center">
                        <img className="mt-12 w-full rounded-full lg:h-48 lg:w-48 h-36 w-36" src={`${user.profile_picture}`} alt="profile_picture" />
                    </div>
                    <div className="px-6 mt-12">
                        <p className="text-gray-700 text-base text-center">{user.position}</p>
                        <p className="font-bold text-2xl text-center">{user.firstname} {user.lastname}</p>
                    </div>
                </div>
                {/* user data */}
                <div className="overflow-x-auto relative lg:mt-5 py-3">
                    <table className="w-auto font-medium lg:text-xl text-left text-gray-500 dark:text-gray-400 mt-4 py-4 font-kanit">
                        <thead className="py-3 px-6 mb-1 text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                            <tr>
                                <th scope="col" className="text-3xl py-3 px-6 rounded-l-lg ">
                                    <h2>ข้อมูลส่วนตัว</h2>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row" className="py-2 px-6 font-medium lg:text-xl text-gray-900 whitespace-nowrap dark:text-white">
                                    ชื่อ :
                                </th>
                                <td className="py-2 px-10">
                                    <input className="border pl-2" value={user.firstname} disabled />
                                    {/* {user.firstname} */}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" className="py-2 px-6 font-medium lg:text-xl text-gray-900 whitespace-nowrap dark:text-white">
                                    นามสกุล :
                                </th>
                                <td className="py-2 px-10">
                                    <input className="border pl-2" value={user.lastname} disabled />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" className="py-2 px-6 font-medium lg:text-xl text-gray-900 whitespace-nowrap dark:text-white">
                                    ชื่อผู้ใช้งาน :
                                </th>
                                <td className="py-2 px-10">
                                    <input className="border pl-2" value={user.username} disabled />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" className="py-2 px-6 font-medium lg:text-xl text-gray-900 whitespace-nowrap dark:text-white">
                                    รหัสผ่าน :
                                </th>
                                <td className="py-2 px-10">
                                    <input className="border pl-2" value={user.password} disabled />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" className="py-2 px-6 font-medium lg:text-xl text-gray-900 whitespace-nowrap dark:text-white">
                                    อีเมล :
                                </th>
                                <td className="py-2 px-10 ">
                                    <input className="border pl-2" value={user.email} disabled />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" className="py-2 px-6 font-medium lg:text-xl text-gray-900 whitespace-nowrap dark:text-white">
                                    เบอร์โทร :
                                </th>
                                <td className="py-2 px-10">
                                    <input className="border pl-2" value={user.phone} disabled />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" className="py-2 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                    ตำแหน่ง :
                                </th>
                                <td className="py-2 px-10">
                                    <input className="border pl-2" value={user.position} disabled />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>


            </div>
        </>
    )
}