import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import { useEffect } from 'react'
import TableDetail from './TableProfile'
import ChangePasswordPage from './ChangePasswordPage';
import UpdateUserForm from './UpdateProfile';


export default function NabTabs({ value, setUser, setValue, user }) {
  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue);
  };
  useEffect(() => {
    if (value == '3') {
      setValue(value)
    }
  }, [value]);

  return (
    <Box sx={{ width: '100%', typography: 'body1' }}>
      <TabContext value={value}>
        <Box sx={{ borderColor: 'divider' }}>
          <TabList onChange={handleChange} aria-label="lab API tabs example">
            <Tab sx={{ fontSize: 18, px: 3 }} label="รายละเอียด" value="1" />
            <Tab sx={{ fontSize: 18, px: 3 }} label="แก้ไข้ข้อมูล" value="3" />
            <Tab sx={{ fontSize: 18, px: 3 }} label="เปลี่ยนรหัสผ่าน" value="2" />

          </TabList>
        </Box>
        <TabPanel value="1"><TableDetail user={user} /></TabPanel>
        <TabPanel value="2"><ChangePasswordPage user={user} setUser={setUser} setValue={setValue} /></TabPanel>
        <TabPanel value="3"><UpdateUserForm user={user} setUser={setUser} setValue={setValue} /></TabPanel>
      </TabContext>
    </Box>
  );
}