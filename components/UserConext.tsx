import { createContext } from "react";

export type AuthUser = {
  id: number,
  username: string,
  password: string,
  firstname: string,
  lastname: string,
  email: string,
  phone: string,
  position: string
}

type UserConextType = {
  user: AuthUser | null
  setUser: React.Dispatch<React.SetStateAction<AuthUser | null>>
}

export const UserContext = createContext({} as UserConextType);

export type IdUser = {
  id: number,
}

type IdUserConextType = {
  user: IdUser | null
  setUser: React.Dispatch<React.SetStateAction<IdUser | null>>
}

export const IdContext = createContext({} as IdUserConextType)