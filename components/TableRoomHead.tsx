import { useState } from "react";
import { MdOutlineExpandLess, MdOutlineExpandMore, MdUnfoldMore } from "react-icons/md";
const TableHead = ({ columns, handleSorting }) => {
  const [columnsName,setCoumnsName] = useState<any>(columns)
  const [sortField, setSortField] = useState<any>("");
  const [order, setOrder] = useState<any>("");

  const widthOfHeader = (accessor) => {
    if(accessor == 'ชื่อที่พัก'){
      return "w-64"
    }else if(accessor == 'จังหวัด'){
      return "w-40"
    }else if(accessor == 'ชื่อผู้ติดต่อ'){
      return "w-56"
    }else if(accessor == ''){
      return "w-36"
    }else {
      return ""
    }
  }

  const handleSortingChange = (sortTable, accessor,index) => {
    if (sortTable) {
      
      const sortOrder =
        accessor === sortField && order === "asc" ? "desc" : "asc";
      setSortField(accessor);
      setOrder(sortOrder);
      const data = [...columnsName]
      data.map((item,id) => {
        if(id != index) {
          data[id].currentOrder = ""
        }else{
          data[index].currentOrder = sortOrder
        }
      })
      handleSorting(accessor, sortOrder);
    }
  };

  return (
    <thead>
      <tr>
        {columnsName.map(({ label, accessor, sortable, currentOrder },index) => {
          const cl = sortable
            ? sortField === accessor && order === "asc"
              ? "up"
              : sortField === accessor && order === "desc"
                ? "down"
                : "default"
            : "";
          if (accessor != "action") {
            return (
              <th
                key={accessor}
                // onClick={sortable ? () => handleSortingChange(accessor) : null}
                onClick={(sortable) => handleSortingChange(sortable, accessor,index)}
                className={`${cl} px-3 py-3 border-b-2 ${widthOfHeader(accessor)} border-gray-200 bg-gray-300 text-center text-md font-semibold text-gray-600 uppercase tracking-wider`}
              >
                <span className=" flex">{label}
                  {currentOrder == "asc" && <MdOutlineExpandLess size={20}></MdOutlineExpandLess>}
                  {currentOrder == "desc" && <MdOutlineExpandMore size={20} ></MdOutlineExpandMore>}
                </span>
              </th>

            );
          } else {
            return (
              <th
                key={accessor}
                className={`${cl} px-3 py-3 border-b-2 border-gray-200 bg-gray-300 text-center text-sm font-semibold text-gray-600 uppercase tracking-wider`}
              >
              
              </th>
            );
          }

        })}
      </tr>
    </thead>
  );
};

export default TableHead;