import { useRouter } from 'next/router'
import { useState, useEffect } from 'react'
import { BiEdit, BiTrashAlt } from 'react-icons/bi'
import { GrView } from 'react-icons/gr'

const TableBody = ({ currentShowItems, columns, changeDataInLocal, setChageDataInLocal, setCurrentShowItem }) => {
  const router = useRouter()
  const [userDataFromLocalForDelete, setUserDataFromLocalForDelete] = useState<any>([])
  const [checkWhenDelete, setCheckWhenDelete] = useState<any>(false)
  const arrayOfState = ["Not-Started", "Confirm", "Reject", "Waiting Info", "In Process"]
  const arrayOfTypeContact = ["Walk in", "Online"]
  const [showCheckDelete, setshowCheckDelete] = useState(false);
  const [checkIndexDelete, setCheckIndexDelete] = useState({ state: false, index: 0 })


  useEffect(() => {
    const data = localStorage.getItem("userDataAll") ? JSON.parse(localStorage.getItem("userDataAll")!) : null;
    if (data) {
      setUserDataFromLocalForDelete(data)
    }
  }, [])

  useEffect(() => {
    if (checkWhenDelete == true) {
      localStorage.setItem('userDataAll', JSON.stringify(userDataFromLocalForDelete));
      setCheckWhenDelete(false)
    }
  }, [checkWhenDelete])

  useEffect(() => {
  }, [userDataFromLocalForDelete])

  useEffect(() => {
    if (changeDataInLocal == true) {
      const data = localStorage.getItem("userDataAll") ? JSON.parse(localStorage.getItem("userDataAll")!) : null;
      if (data) {
        setUserDataFromLocalForDelete(data)
        setChageDataInLocal(false)
      }
    }
  }, [changeDataInLocal])

  // set data for view page
  const handleViewLead = (item, index) => {
    localStorage.setItem('currentUserData', JSON.stringify(item));
    router.push("./viewSalesPage")

  }

  // set data for edit page
  const handleEditLead = (item, index) => {
    localStorage.setItem('currentUserData', JSON.stringify(item));
    router.push("./editSalesPage")
  }

  useEffect(() => {
    if (checkIndexDelete.state == true) {
      let array = [...userDataFromLocalForDelete]; // make a separate copy of the array
      if (array.length == 1) {
        setCurrentShowItem([])
        localStorage.setItem('showDataUser', JSON.stringify([]))
      }
      if (checkIndexDelete.index !== -1) {
        array.splice(checkIndexDelete.index, 1);
        setUserDataFromLocalForDelete(array)
        setCheckWhenDelete(true)
        setChageDataInLocal(true)
      } setCheckIndexDelete({ ...checkIndexDelete, state: false })
    }
  }, [checkIndexDelete])

  const handleDeleteLead = (indexDelete) => {
    setCheckIndexDelete({ ...checkIndexDelete, index: indexDelete })
    setshowCheckDelete(true)
  }

  const backgroundTypeContact = (type) => {
    switch (type) {
      case "Walk in":
        return "bg-violet-300"

      case "Online":
        return "bg-blue-300"

      default:
        return ""
    }
  }

  const backgroundStatus = (status) => {
    switch (status) {
      case "Confirm":
        return "bg-green-200"

      case "Reject":
        return "bg-red-200"

      case "Waiting Info":
        return " bg-orange-200"

      case "In Process":
        return "bg-blue-200"

      case "Not-Started":
        return "bg-yellow-200"

      default:
        return ""
    }
  }

  return (
    <>
      <tbody>
        {currentShowItems && currentShowItems.map((data, index) => {
          return (
            <tr key={data.id}>
              {columns.map(({ accessor }) => {
                const tData = data[accessor] ? data[accessor] : "——";
                if (arrayOfState.includes(tData) || arrayOfTypeContact.includes(tData)) {
                  return (
                    <td className="text-center px-3 py-5 border-b border-gray-200 bg-gray-100 text-sm ">
                      <span
                        className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight border-gray-200 ">
                        <span aria-hidden
                          className={`${backgroundStatus(tData)} ${backgroundTypeContact(tData)} absolute inset-0 opacity-50 rounded-full `}></span>
                        <span className="relative">
                          {tData}
                        </span>
                      </span>
                    </td>
                  )
                  // return <td key={accessor} className="px-3 py-5 border-b border-gray-200 bg-white text-sm md:text-base">{tData}</td>;
                } else if (tData != "——")
                  return <td key={accessor} className="px-3 py-5 border-b border-gray-200 bg-gray-100 text-sm md:text-base">{tData}</td>;
                else {
                  return (<td className="px-3 py-5 border-b border-gray-200 bg-gray-100 text-sm">
                    <button onClick={() => { handleViewLead(data, index) }} className="cursor px-1" ><GrView size={20} color={"rgb(34,197,94)"}></GrView></button>
                    <button onClick={() => { handleEditLead(data, index) }} className="cursor px-1" ><BiEdit size={20} color={"rgb(34,197,94)"}></BiEdit></button>
                    <button onClick={() => { handleDeleteLead(index) }} className="cursor px-1" ><BiTrashAlt size={20} color={"rgb(244,63,94)"}></BiTrashAlt></button>
                  </td>)

                }
              })}
            </tr>
          )
        })}
      </tbody>
      {showCheckDelete == true ? (<><div
        className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
        <div className="relative w-auto mt-6 mb-2 mx-auto max-w-sm">
          {/*content*/}
          <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
            {/*body*/}
            <div className="relative p-6 flex-auto">
              <p className="my-3 text-slate-500 text-lg leading-relaxed">
                คุณต้องการลบข้อมูลใช่หรือไม่?
              </p>
            </div>
            {/*footer*/}
            <div className="flex items-center justify-end p-5 border-solid border-slate-200 rounded-b">
              <button
                className="bg-green-600 text-white active:bg-rose-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                type="button"
                onClick={() => {
                  setCheckIndexDelete({ ...checkIndexDelete, state: true })
                  setshowCheckDelete(false)
                }}
              >ใช่</button>
              <button
                className="bg-rose-600 text-white active:bg-rose-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                type="button"
                onClick={() => { setshowCheckDelete(false) }}
              >ปิด</button>
            </div>
          </div>
        </div>
      </div>
        <div className="opacity-25 fixed inset-0 z-40 bg-black"></div></>) : <></>}
    </>
  );
};

export default TableBody;