import React from 'react'
import { BsCheckCircle } from 'react-icons/bs'


export default function ValidatePassword({ color1,color2,color3 }) {
  return (
    <>
      <div className='md:px-5 md: py-24 '>
        <div className='flex items-center p-3 pl-5 text-base'>
          <BsCheckCircle size='20' className={color1}/>
          <span className={`${color1} ml-6 text-xs`}> รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร </span>
        </div>
        <div className='flex items-center p-3 pl-5 text-base'>
          <BsCheckCircle size='20' className={color2}/>
          <span className={`${color2} ml-6 text-xs`}> มีตัวอักษรพิมพ์ใหญ่และพิมพ์เล็ก </span>
        </div>
        <div className='flex items-center p-3 pl-5 text-base'>
          <BsCheckCircle size='20' className={color3}/>
          <span className={`${color3} ml-6 text-xs`}> ตัวเลข (0-9) </span>
        </div>
      </div>
    </>
  )
}