import { useState } from "react"
import NullInput from './NullnputMassage'

const formReducer = (state, event) => {
    return {
        ...state,
        [event.target.name]: event.target.value
    }
}

export default function UpdateUserForm({ user, setUser, setValue }) {

    const [formEdit, setFormEdit] = useState<any>(user)

    const notNullInput = formEdit.firstname
        && formEdit.lastname
        && formEdit.email
        && formEdit.phone
        && formEdit.position

    const handleSubmit = () => {
        if (notNullInput) {
            localStorage.setItem('userLogin', JSON.stringify(formEdit))
            setUser(formEdit)
            setValue('1')
        }
    }

    const handleBack = () => {
        setValue('1')
    }


    return (
        <>
            <div className=" mt-4 lg:mt-5 mx-auto w-max py-3"></div>
            <div>
                <h2 className="py-3 px-6 mb-1 text-3xl font-bold leading-tight tracking-tight text-gray-900 dark:text-white">
                    แก้ไข้ข้อมูล
                </h2>
            </div>
            <div className="grid grid-cols-6 gap-4 grid-flow-col ">
                <div className="grid">
                    <label className='px-6 font-medium lg:text-xl text-gray-900 whitespace-nowrap dark:text-white'>ชื่อ</label>
                </div>
                <div className="col-start-2 col-span-2">
                    <input type="text"
                        autoComplete='off'
                        name="firstname"
                        value={formEdit.firstname}
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm lg:text-xl w-36 rounded-sm focus:ring-blue-500 focus:border-blue-500 block md:w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 ml-5 h-9"
                        placeholder="ชื่อ"
                        onChange={e => {
                            setFormEdit({ ...formEdit, firstname: e.target.value })
                        }} />
                </div>
                <div className="col-start-4 col-span-2">
                    {!formEdit.firstname ? <NullInput message='ชื่อ'></NullInput> : <div className="pt-1"></div>}
                </div>

                <div className="grid">
                    <label className='px-6 font-medium lg:text-xl text-gray-900 whitespace-nowrap dark:text-white'>นามสกุล</label>
                </div>
                <div className="col-start-2 col-span-2">
                    <input type="text"
                        autoComplete='off'
                        name="lastname"
                        value={formEdit.lastname}
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm lg:text-xl w-36 rounded-sm focus:ring-blue-500 focus:border-blue-500 block md:w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 ml-5 h-9"
                        placeholder="นามสกุล"
                        onChange={e => {
                            setFormEdit({ ...formEdit, lastname: e.target.value })
                        }} />
                </div>
                <div className="col-start-4 col-span-2">
                    {!formEdit.lastname ? <NullInput message='นามสกุล'></NullInput> : <></>}
                </div>

                <div className="grid">
                    <label className='px-6 font-medium lg:text-xl text-gray-900 whitespace-nowrap dark:text-white'>อีเมล์</label>

                </div>
                <div className="col-start-2 col-span-2">
                    <input type="text"
                        autoComplete='off'
                        name="email"
                        value={formEdit.email}
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm lg:text-xl w-36 rounded-sm focus:ring-blue-500 focus:border-blue-500 block md:w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 ml-5 h-9"
                        placeholder="อีเมล์"
                        onChange={e => {
                            setFormEdit({ ...formEdit, email: e.target.value })
                        }} />
                </div>
                <div className="col-start-4 col-span-2">
                    {!formEdit.email ? <NullInput message='อีเมล์'></NullInput> : <></>}
                </div >
                <div className="grid">
                    <label className='px-6 font-medium lg:text-xl text-gray-900 whitespace-nowrap dark:text-white'>เบอร์โทร</label>
                </div>
                <div className="col-start-2 col-span-2">
                    <input type="text"
                        autoComplete='off'
                        name="phone"
                        value={formEdit.phone}
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm lg:text-xl w-36 rounded-sm focus:ring-blue-500 focus:border-blue-500 block md:w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 ml-5 h-9"
                        placeholder="เบอร์โทร"
                        onChange={e => {
                            setFormEdit({ ...formEdit, phone: e.target.value })
                        }} />
                </div>
                <div className="col-start-4 col-span-2">
                    {!formEdit.phone ? <NullInput message='เบอร์โทร'></NullInput> : <></>}
                </div >
                <div className="grid">
                    <label className=' px-6 font-medium lg:text-xl text-gray-900 whitespace-nowrap dark:text-white'>ตำแหน่ง</label>
                </div>
                <div className="col-start-2 col-span-2">
                    <select name="position" placeholder="Choose a position" value={formEdit.position}
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm lg:text-xl w-36 rounded-sm focus:ring-blue-500 focus:border-blue-500 block md:w-40 p-1 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 ml-5 h-9"
                        onChange={e => {
                            setFormEdit({ ...formEdit, position: e.target.value })
                        }}>
                        <option value="superadmin">SuperAdmin</option>
                        <option value="admin">Admin</option>
                        <option value="employee">Employee</option>
                    </select>
                </div>
                <div className="col-start-4 col-span-2">
                    {!formEdit.position || formEdit.position == "" ? <NullInput message='ตำแหน่ง'></NullInput> : <></>}
                </div >
            </div >
            <div className="flex justify-end pt-3">
                <button onClick={handleSubmit} className="text-white mx-3 bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-auto sm:w-24 px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">บันทึก</button>
                <button onClick={handleBack} className="text-white mx-3 bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-auto sm:w-24 px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">กลับ</button>
            </div>
        </>
    )
}