import * as FileSaver from 'file-saver';
import XLSX from 'sheetjs-style';
import { Tooltip } from '@mui/material';
import { MdFileDownload } from 'react-icons/md';

export default function ExcelExport({ excelData, fileName }) {
  const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  const fileExtension = '.xlsx';

  const exportToExcel = (excelData, fileName) => {
    const ws = XLSX.utils.json_to_sheet(excelData);
    const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
    const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
    const data = new Blob([excelBuffer], { type: fileType });
    FileSaver.saveAs(data, fileName + fileExtension);
  };

  return (
    <button title='ดาวน์โหลดข้อมูลเป็นไฟล์ Excel' className=' p-2 text-md bg-blue-600 hover:bg-blue-700 w-auto rounded-md text-white'
    onClick={(e) => exportToExcel(excelData, fileName)}
    ><MdFileDownload size={22} ></MdFileDownload>
    </button>
  )
}