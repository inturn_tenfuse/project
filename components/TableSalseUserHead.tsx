import { useState } from "react";
import { MdOutlineExpandLess, MdOutlineExpandMore, MdUnfoldMore } from "react-icons/md";
const TableHead = ({ columns, handleSorting }) => {
  const [columnsName,setCoumnsName] = useState<any>(columns)
  const [sortField, setSortField] = useState<any>("");
  const [order, setOrder] = useState<any>("");

  const widthOfHeader = (accessor) => {
    if(accessor == 'id'){
      return "w-16"
    }else if(accessor == 'firstname'){
      return "w-36"
    }else if(accessor == 'lastname'){
      return "w-36"
    }else if(accessor == 'email'){
      return "w-56 lg:w-80"
    }else if(accessor == 'phone'){
      return "w-36 lg:w-44"
    }else if(accessor == 'position'){
      return "w-36 lg:w-44"
    }else {
      return ""
    }
  }

  const handleSortingChange = (sortTable, accessor,index) => {
    if (sortTable) {
      
      const sortOrder =
        accessor === sortField && order === "asc" ? "desc" : "asc";
      setSortField(accessor);
      setOrder(sortOrder);
      const data = [...columnsName]
      data.map((item,id) => {
        if(id != index) {
          data[id].currentOrder = ""
        }else{
          data[index].currentOrder = sortOrder
        }
      })
      handleSorting(accessor, sortOrder);
    }
  };

  return (
    <thead>
      <tr>
        {columnsName.map(({ label, accessor, sortable, currentOrder },index) => {
          const cl = sortable
            ? sortField === accessor && order === "asc"
              ? "up"
              : sortField === accessor && order === "desc"
                ? "down"
                : "default"
            : "";
          if (accessor != "action") {
            return (
              <th
                key={accessor}
                // onClick={sortable ? () => handleSortingChange(accessor) : null}
                onClick={(sortable) => handleSortingChange(sortable, accessor,index)}
                className={`${cl} px-3 py-3 border-b-2 ${widthOfHeader(accessor)} border-gray-200 bg-gray-300 text-center text-md font-semibold text-gray-600 uppercase tracking-wider`}
              >
                <span className=" flex">{label}
                  {currentOrder == "asc" && <MdOutlineExpandLess size={20}></MdOutlineExpandLess>}
                  {currentOrder == "desc" && <MdOutlineExpandMore size={20} ></MdOutlineExpandMore>}
                </span>
              </th>

            );
          } else {
            return (
              <th
                key={accessor}
                className={`${cl} px-3 py-3 border-b-2 border-gray-200 bg-gray-300 text-center text-sm font-semibold text-gray-600 uppercase tracking-wider`}
              >
              
              </th>
            );
          }

        })}
      </tr>
    </thead>
  );
};

export default TableHead;