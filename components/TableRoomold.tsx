import React, { useState, useEffect } from 'react'
import { BiEdit, BiTrashAlt } from 'react-icons/bi'
import { GrView } from 'react-icons/gr'
import * as xlsx from 'xlsx';

export default function TableRoom({ setPage, excelDataFromLocal, setExcelDataFromLocal, checkChangeData, setCheckChangeData, setDataItemFromClick, setCurrentIndex }) {
  const initialSearch = ""
  const [excelFileError, setExcelFileError] = useState<any>("please select your excel file");
  const [excelData, setExcelData] = useState<any>([]);
  const [filteredData, setFilteredData] = useState<any>(initialSearch);




  const handleAddLead = () => {
    setPage("2")
  }

  // set data for view page
  const handleViewLead = (item, index) => {
    setDataItemFromClick(item)
    setCurrentIndex(index)
    setPage("1")
  }

  // set data for edit page
  const handleEditLead = (item, index) => {
    setDataItemFromClick(item)
    setCurrentIndex(index)
    setPage("3")
  }

  useEffect(() => {
    if (excelDataFromLocal) {
      setFilteredData(excelDataFromLocal)
    }
  }, [])

  useEffect(() => {
    if (excelDataFromLocal) {
      setFilteredData(excelDataFromLocal)
    }
  }, [excelDataFromLocal]);

  useEffect(() => {
    const data = localStorage.getItem("roomDetail") ? JSON.parse(localStorage.getItem("roomDetail")!) : null;
    setExcelDataFromLocal(data);
    setCheckChangeData(false)
  }, [checkChangeData]);

  const handleDeleteLead = (index) => {
    var array = [...excelDataFromLocal]; // make a separate copy of the array
    if (index !== -1) {
      array.splice(index, 1);
      setExcelDataFromLocal(array)
    }
  }

  const backgroundStatus = (status) => {
    switch (status) {
      case "Confirm":
        return "bg-green-200"

      case "Reject":
        return "bg-red-200"

      case "Waiting Info":
        return " bg-orange-200"

      case "In Process":
        return "bg-blue-200"

      case "Not-Started":
        return "bg-yellow-200"

      default:
        return (<></>)
    }
  }

  //file
  const readExcel = async (e) => {
    const file = e?.target.files[0];
    if (file) {
      setExcelFileError("")
      const data = await file.arrayBuffer(file);
      const excelfile = xlsx.read(data, {
        cellText: false, cellDates: true
      });
      const excelsheet = excelfile.Sheets[excelfile.SheetNames[0]];
      const exceljson = xlsx.utils.sheet_to_json(excelsheet, { raw: false, dateNF: 'yyyy-mm-dd' });
      // console.log(data);
      // console.log(excelfile);
      // console.log(excelsheet);
      // console.log(exceljson);
      setExcelData(exceljson);
    } else {
      setExcelFileError("please select your excel file")
    }
  }

  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    result = excelDataFromLocal.filter((data) => {
      return data.ชื่อที่พัก.search(value) != -1;
    });
    setFilteredData(result);
  }

  const clickReadExcel = () => {
    setCheckChangeData(true)
    if (excelData != null) {
      localStorage.setItem('roomDetail', JSON.stringify(excelData));
    }
  }

  return (
    <div className=" p-8 rounded-md w-full">
      <div className=" flex items-center justify-end pb-6 mr-10">
        <div className="flex items-center justify-end">
          <button onClick={handleAddLead} className="bg-blue-500 hover:bg-blue-600 px-4 py-2 w-auto sm:w-32 rounded-md text-white font-semibold tracking-wide cursor-pointer">เพิ่มข้อมูล</button>
        </div>
      </div>
      <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
        <div className='py-3'>
          <div className='inline-flex'>
            <div className="justify-start">
              <span className="sr-only">Choose File</span>
              <input type="file" className=" w-56 text-sm text-gray-500 bg-gray-200 file:mr-4 file:py-2 file:px-4 file:rounded-lg file:border-0 file:text-sm file:font-semibold file:bg-gray-300 file:text-black hover:file:bg-gray-400"
                accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                onChange={readExcel} required />
            </div>
            <div className="flex items-end  mr-3">
              <label className="sr-only">Search</label>
              <div className=" relative w-48">
                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                  <svg aria-hidden="true" className="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path></svg>
                </div>
                <input type="text" onChange={(event) => handleSearch(event)} id="simple-search" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Search" required />
              </div>
              <button type="submit" className="p-2.5 ml-2 text-sm font-medium text-white bg-blue-700 rounded-lg border border-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                <span className="sr-only">Search</span>
              </button>
            </div>
          </div>

          {excelFileError && !excelDataFromLocal ? <div className='text-rose-600 ml-28'>{excelFileError}</div> : <div className='mt-3'></div>}



          <div className='pb-5 pt-1'>
            <button className='text-md bg-blue-500 hover:bg-blue-600 py-1 w-auto sm:w-32 rounded-md text-white font-semibold tracking-wide cursor-pointer'
              onClick={clickReadExcel}> export </button>
          </div>

          <div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
            <table className="min-w-full leading-normal">
              <thead>
                <tr>
                  <th
                    className="px-3 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-sm font-semibold text-gray-600 uppercase tracking-wider">
                    ชื่อที่พัก
                  </th>
                  <th
                    className="px-3 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-sm font-semibold text-gray-600 uppercase tracking-wider">
                    จังหวัด
                  </th>
                  <th
                    className="px-3 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-sm font-semibold text-gray-600 uppercase tracking-wider">
                    ชื่อผู้ติดต่อ
                  </th>
                  <th
                    className="px-3 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-sm font-semibold text-gray-600 uppercase tracking-wider">
                    กิจกรรม
                  </th>
                  <th
                    className="px-3 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-sm font-semibold text-gray-600 uppercase tracking-wider">
                    สถานะ
                  </th>
                  <th
                    className="px-3 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                  </th>
                </tr>
              </thead>
              {filteredData && filteredData.map((item, index) => {
                return (
                  <tbody>
                    <tr>
                      <td className="px-3 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap w-24 md:w-36 break-words">
                          {item.ชื่อที่พัก}
                        </p>
                      </td>
                      <td className="px-3 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap">
                          {item.จังหวัด}
                        </p>
                      </td>
                      <td className="px-3 py-5 border-b border-gray-200 bg-white text-sm text-left">
                        <p className="text-gray-900 whitespace-no-wrap">
                          {item.ชื่อผู้ติดต่อ}
                        </p>
                      </td>
                      <td className="px-3 py-5 border-b border-gray-200 bg-white text-sm text-center">
                        <p className="text-gray-900 whitespace-no-wrap">
                          {item.กิจกรรม}
                        </p>
                      </td>
                      <td className="text-center px-3 py-5 border-b border-gray-200 bg-white text-sm ">
                        <span
                          className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight ">
                          <span aria-hidden
                            className={`${backgroundStatus(item.สถานะการจอง)} absolute inset-0 opacity-50 rounded-full `}></span>
                          <span className="relative">
                            {item.สถานะการจอง}
                          </span>
                        </span>
                      </td>
                      <td className="px-3 py-5 border-b border-gray-200 bg-white text-sm">
                        <button onClick={e => { handleViewLead(item, index) }} className="cursor px-1" ><GrView size={20} color={"rgb(34,197,94)"}></GrView></button>
                        <button onClick={e => { handleEditLead(item, index) }} className="cursor px-1" ><BiEdit size={20} color={"rgb(34,197,94)"}></BiEdit></button>
                        <button onClick={() => { handleDeleteLead(index) }} className="cursor px-1" ><BiTrashAlt size={20} color={"rgb(244,63,94)"}></BiTrashAlt></button>
                      </td>
                    </tr>
                  </tbody>
                );
              }

              )}
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}