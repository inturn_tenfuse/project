import React, { useEffect, useState } from 'react'
import NullInput from './NullnputMassage'
import ValidatePassword from './ValidatePassword'
export default function ChangePasswordPage({ user, setUser, setValue }: any) {
  const initailInputPassword = { oldPassword: "", newPassword: "", validatePassword: "" }

  const [check, setCheck] = useState<any>(false)
  const [inputPassword, setInputPassword] = useState<any>(initailInputPassword)
  const [colorIcon1, setIcon1] = useState<any>('text-rose-500')
  const [colorIcon2, setIcon2] = useState<any>('text-rose-500')
  const [colorIcon3, setIcon3] = useState<any>('text-rose-500')

  const matchValidatePassword = inputPassword.newPassword == inputPassword.validatePassword
  const nullValidatePassword = inputPassword.validatePassword == ''

  useEffect(() => {
    const items = localStorage.getItem("userLogin") ? JSON.parse(localStorage.getItem("userLogin")!) : "";
    if (items) {
      setUser(items);
    }
  }, []);

  const handleBack = () => {
    setValue('1')
  }

  function handleChangePassword() {
    const viaOldPassword = (user.password === inputPassword.oldPassword)
    const oldNewPasswordNoSame = (inputPassword.oldPassword !== inputPassword.newPassword)
    const viaNewPassword = (inputPassword.newPassword === inputPassword.validatePassword)
    const conditionNewPassword = colorIcon1 == 'text-green-500' && colorIcon2 == 'text-green-500' && colorIcon3 == 'text-green-500'
    if (conditionNewPassword && viaNewPassword && viaOldPassword && oldNewPasswordNoSame) {
      setUser({ ...user, password: inputPassword.newPassword })
      setCheck(true)
    }
  }

  function checkLengthNewPassword() {
    const limitLength = inputPassword.newPassword.length >= 8 && inputPassword.newPassword.length <= 25
    if (limitLength) {
      setIcon1('text-green-500')
    }
    if (!limitLength) {
      setIcon1('text-rose-500')
    }
  }

  function checkUpperLowerPassword() {
    const lowerCase = /^(?=.*[a-z])/.test(inputPassword.newPassword)
    const upperCase = /^(?=.*[A-Z])/.test(inputPassword.newPassword)
    if (lowerCase && upperCase) {
      setIcon2('text-green-500')
    }
    if (!lowerCase || !upperCase) {
      setIcon2('text-rose-500')
    }
  }

  function checkDigitPassword() {
    const checkDigit = /\d/.test(inputPassword.newPassword)
    if (checkDigit) {
      setIcon3('text-green-500')
    }
    if (!checkDigit) {
      setIcon3('text-rose-500')
    }
  }

  useEffect(() => {
    checkUpperLowerPassword()
    checkLengthNewPassword()
    checkDigitPassword()
  }, [inputPassword.newPassword]);


  useEffect(() => {
    if (check === true && user.password != inputPassword.oldPassword) {
      localStorage.setItem('userLogin', JSON.stringify(user))
      setInputPassword(initailInputPassword)
    }
  }, [user.password]);

  return (
    <>
      <div className='flex w-max'>
        <div className=" mt-4 lg:mt-6 py-5 ">
          <div>
            <h2 className="py-3 px-6 mb-1 text-3xl font-bold leading-tight tracking-tight text-gray-900 dark:text-white">
              เปลี่ยนรหัสผ่าน
            </h2>
          </div>
          <div className="grid grid-cols-2 gap-4 grid-flow-col">
            <div className="grid">
              <label className='py-1 px-6 font-medium lg:text-xl text-gray-900 whitespace-nowrap dark:text-white'>รหัสผ่านเดิม</label>
            </div>
            <div className="col-start-2">
              <div className='grid grid-row-2'>
                <input type="password"
                  autoComplete='off'
                  name="oldPassword"
                  value={inputPassword.oldPassword}
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm lg:text-xl rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-9"
                  placeholder="********"
                  onChange={e => {
                    setInputPassword({ ...inputPassword, oldPassword: e.target.value })
                  }} />
              </div>
              <div className="">
                {!inputPassword.oldPassword ? <NullInput message='รหัสผ่าน'></NullInput> : <div className='py-3'></div>}
              </div>
            </div>


            <div className="grid">
              <label className='py-1 px-6 font-medium lg:text-xl text-gray-900 whitespace-nowrap dark:text-white'>รหัสผ่านใหม่</label>
            </div>
            <div className="col-start-2">
              <div className='grid grid-row-2'>
                <input type="password"
                    autoComplete='off'
                    name="newPassword"
                    value={inputPassword.newPassword}
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm lg:text-xl rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-9"
                    placeholder="********"
                    onChange={e => {
                      setInputPassword({ ...inputPassword, newPassword: e.target.value })
                    }} />
              </div>
              <div className="">
                {!inputPassword.newPassword ? <NullInput message='รหัสผ่าน'></NullInput> : <div className='py-3'></div>}
              </div>
            </div>

            <div className="grid">
              <label className='py-1 px-6 font-medium lg:text-xl text-gray-900 whitespace-nowrap dark:text-white'>ยืนยันรหัสผ่าน</label>
            </div>
            <div className="col-start-2">
              <div className='grid grid-row-2'>
                <input type="password"
                    autoComplete='off'
                    name="validatePassword"
                    value={inputPassword.validatePassword}
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm lg:text-xl rounded-sm focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dar ml-5k:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 h-9"
                    placeholder="********"
                    onChange={e => {
                      setInputPassword({ ...inputPassword, validatePassword: e.target.value })
                    }} />
              </div>
              <div >
                  {!matchValidatePassword && !nullValidatePassword ? <span className='text-rose-600 text-xs ml-6'>*** รหัสผ่านไม่ตรงกัน </span> : <></>}
                  {nullValidatePassword ? <NullInput message='รหัสผ่าน'></NullInput> : <div className='py-3'></div>}

                </div>
            </div>
          </div>

        </div>
        <div>
          <ValidatePassword color1={colorIcon1} color2={colorIcon2} color3={colorIcon3}></ValidatePassword>
        </div>
      </div>
      <div className="flex justify-end pt-3">
        <button onClick={handleChangePassword} className="text-white mx-3 bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-auto sm:w-24 px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">บันทึก</button>
        <button onClick={handleBack} className="text-white mx-3 bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-auto sm:w-24 px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">กลับ</button>
      </div>
    </>
  )
}