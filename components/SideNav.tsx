import Link from 'next/link';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import { SidebarData } from '../data/SidebarData';

export default function SideNav() {
  const router = useRouter();
  const [sidebarData, setSidebarData] = useState<any>(SidebarData)
  const [currentTab, setCurrentTab] = useState<any>(null)


  // useEffect(() => {
  //   const data = localStorage.getItem("currentTab") ? (localStorage.getItem("currentTab")!) : null;
  //   if (data != null) {
  //     setCurrentTab(data)
  //   } else {
  //     setCurrentTab("หนัาหลัก")
  //   }
  // }, [])

  useEffect(() => {
    const data = localStorage.getItem("currentTab") ? (localStorage.getItem("currentTab")!) : null;
    if (data != null) {
      setCurrentTab(data)
    }
  }, [currentTab])


  const handleClick = (title, onHandleClick, indexCurrent) => {
    if (title == "ออกจากระบบ") {
      localStorage.removeItem('currentTab')
    } else {
      localStorage.setItem('currentTab', title)
    }
    setCurrentTab(indexCurrent);
    if (onHandleClick === "/logout") {
      localStorage.removeItem('userLogin')
    }
  };

  return (
    <>
      <main className='flex flex-row justify-start'>
        <div className='fixed h-screen p-3'>
          <div className='h-full overflow-y-auto py-4  bg-blue-500 rounded-xl  dark:bg-gray-800 w-max flex'>
            <nav>
              <div className='my-5'>
                <Link href='/home' className='flex justify-center'>
                  <img src="Logo.svg" className='w-20 h-20' />
                </Link>
              </div>
              <ul className='nav-menu-items'>
                {sidebarData.map((item, index) => {
                  return (
                    <div className='pb-1 hover:text-black'>
                      <li key={index} className={`${currentTab != item.title ? "hover:bg-blue-700" : "hover:bg-blue-900"} ${(currentTab == null && item.title=="หน้าหลัก") ? "hover:bg-blue-900" : ""} rounded-xl mx-2 ${item.title == "หน้าหลัก" && currentTab == null ? "bg-blue-900" : ""} ${item.title == currentTab ? "bg-blue-900" : ""} `}>
                        <Link onClick={() => handleClick(item.title, item.click, index)} href={item.path} className={item.cName}>
                          {item.icon}
                          <span className='px-4 text-lg text-white '>{item.title}</span>
                        </Link>
                      </li>
                    </div>
                  );
                })}
              </ul>
            </nav>
          </div>
        </div>
      </main>
    </>
  )
}