import { useState, useEffect } from 'react'
import ReactPaginate from 'react-paginate';

export default function PaginatedItems({ itemsPerPage, tableData, currentItems, setCurrentItems , nameLocal }) {
  // We start with an empty list of items.

  const [pageCount, setPageCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(1)
  // Here we use item offsets; we could also use page offsets
  // following the API or data you're working with.
  const [itemOffset, setItemOffset] = useState(0);

  useEffect(() => {
    if (currentItems) {
      localStorage.setItem(`${nameLocal}`, JSON.stringify(currentItems))
    }
  }, [currentItems])

  useEffect(() => {
    // Fetch items from another resources.
    if (tableData) {
      const endOffset = itemOffset + itemsPerPage;
      setCurrentItems(tableData.slice(itemOffset, endOffset));
      setPageCount(Math.ceil(tableData.length / itemsPerPage));
    }else{
      setPageCount(0)
      setCurrentItems([])
    }
  }, [itemOffset, itemsPerPage, tableData]);

  // Invoke when user click to request another page.
  const handlePageClick = (event) => {
    if (tableData) {
      const newOffset = event.selected * itemsPerPage % tableData.length;
      setItemOffset(newOffset);
    }
  };

  return (
    <>
      <ReactPaginate
        className='flex justify-center'
        nextLabel="next >"
        onPageChange={handlePageClick}
        pageRangeDisplayed={3}
        marginPagesDisplayed={2}
        pageCount={pageCount}
        previousLabel="< previous"
        pageClassName="page-item px-3 py-2 bg-slate-100 leading-tight text-gray-500 bg-white border border-gray-500 hover:bg-gray-300 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
        pageLinkClassName="page-link"
        previousClassName="page-item px-3 bg-slate-100 py-2 ml-0 leading-tight text-gray-500 bg-white border border-gray-500  rounded-l-lg hover:bg-gray-300 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
        previousLinkClassName="page-link"
        nextClassName="page-item px-3 py-2 bg-slate-100  leading-tight text-gray-500 bg-white border border-gray-500 rounded-r-lg hover:bg-gray-300 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
        nextLinkClassName="page-link"
        breakLabel="..."
        breakClassName="page-item px-3 py-2 bg-slate-100 leading-tight text-gray-500 bg-white border border-gray-500  hover:bg-gray-300 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
        breakLinkClassName="page-link"
        containerClassName="pagination"
        activeClassName="active"
      />
    </>
  );
}