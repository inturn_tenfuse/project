import { useEffect, useState } from "react";
import PaginatedItems from "./Paginated";
import TableBody from "./TableRoomBody";
import TableHead from "./TableRoomHead";
import { useSortableTable } from "./useSortableTable";

const Table = ({ data, columns, changeDataInLocal, setChangeDataInLocal }) => {
  const [currentShowItems, setCurrentShowItems] = useState([]);
  const [tableData, handleSorting] = useSortableTable(data, columns);
  const [checkBlankData, setCheckBlankData] = useState<any>()
  let itemPerPage = 10

  return (
    <>
      <div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
        <table className="min-w-full leading-normal">
          <TableHead {...{ columns, handleSorting }} />
          <TableBody changeDataInLocal={changeDataInLocal} setChageDataInLocal={setChangeDataInLocal} setCurrentShowItem={setCurrentShowItems} {...{ columns, currentShowItems }} />
        </table>
        {/* {currentShowItems.length == 0 && <div className='flex justify-center items-center p-5 bg-gray-100'>No have data</div>} */}
        {/* {excelFileError != null && excelDataFromLocal.length != 0 && excelFileError.length == 0 && <div className='flex justify-center items-center p-5 bg-gray-100'>Data search not have</div>} */}
      </div>
      <div className='pt-5'>
        <PaginatedItems itemsPerPage={itemPerPage} tableData={tableData} currentItems={currentShowItems} setCurrentItems={setCurrentShowItems} nameLocal={"showDataRoom"} />
      </div>
    </>
  );
};

export default Table;