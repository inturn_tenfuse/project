export const HotelData = [
  {
    hotelname: 'โรงแรมริเวอร์',
    province: 'นครปฐม',
    contactname: 'นายอิทธิพล ชิณวังโส',
    activity: [ 'โทรศัพท์' , 'อีเมล์' , 'ทางเว็ปไซต์' , 'ติดต่อทางโรงแรม' ],
    activity_detail: "",
    status: 'Not-Started'
  },
  {
    hotelname: 'โรงแรมบีทู แอร์พอร์ท บูติค แอนด์ บัดเจท',
    province: 'กรุงเทพมหานครฯ',
    contactname: 'นายอิทธิพล ชิณวังโส',
    activity: [ 'โทรศัพท์' , 'อีเมล์' , 'ทางเว็ปไซต์' , 'ติดต่อทางโรงแรม' ],
    activity_detail: "",
    status: 'Reject'
  },
  {
    hotelname: 'โรงแรมเพนนินซูล่า',
    province: 'กรุงเทพมหานครฯ',
    contactname: 'นายอิทธิพล ชิณวังโส',
    activity: [ 'โทรศัพท์' , 'ติดต่อทางโรงแรม' ],
    activity_detail: "",
    status: 'Not-Started'
  },
  {
    hotelname: 'โรงแรมริทซ์ คาลตัน บางกอก',
    province: 'ชลบุรี',
    contactname: 'นายอิทธิพล ชิณวังโส',
    activity: [ 'ทางเว็ปไซต์' , 'ติดต่อทางโรงแรม' ],
    activity_detail: "",
    status: 'Confirm'
  },
  {
    hotelname: 'โรงแรมพาร์ค ไฮแอท เซ็นทรัล แอมบาสซี่',
    province: 'กรุงเทพมหานครฯ',
    contactname: 'นายอิทธิพล ชิณวังโส',
    activity: [ 'โทรศัพท์' , 'ทางเว็ปไซต์' , 'ติดต่อทางโรงแรม' ],
    activity_detail: "",
    status: 'Waiting Info'
  },
  {
    hotelname: 'โรงแรมตรีสรา',
    province: 'ชลบุรี',
    contactname: 'นายอิทธิพล ชิณวังโส',
    activity: [ 'โทรศัพท์' ],
    activity_detail: "",
    status: 'In Process'
  },
];