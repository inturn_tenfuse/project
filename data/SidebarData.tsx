import React from 'react';
import { CgProfile } from "react-icons/cg"
import { AiFillHome, AiOutlineUsergroupAdd } from "react-icons/ai"
import { SlLogout } from "react-icons/sl"
import { MdOutlineManageSearch } from "react-icons/md"


export const SidebarData = [
  {
    title: 'หน้าหลัก',
    path: '/home',
    icon: <AiFillHome color='white' />,
    cName: 'flex items-center px-5 py-2 text-base font-normal text-gray-900 rounded-lg',
    click: '',
  },
  {
    title: 'จัดการที่พัก',
    path: '/roomManagement',
    icon: <MdOutlineManageSearch color='white' />,
    cName: 'flex items-center px-5 py-2 text-base font-normal text-gray-900 rounded-lg ',
    click: '',

  },
  {
    title: 'จัดการบัญชีผู้ใช้',
    path: '/salesUserManagement',
    icon: <AiOutlineUsergroupAdd color='white' />,
    cName: 'flex items-center px-5 py-2 text-base font-normal text-gray-900 rounded-lg ',
    click: '',

  },
  {
    title: 'บัญชีของฉัน',
    path: '/profilePage',
    icon: <CgProfile color='white' />,
    cName: 'flex items-center px-5 py-2 text-base font-normal text-gray-900 rounded-lg ',
    click: '',

  },
  {
    title: 'ออกจากระบบ',
    path: '/',
    icon: <SlLogout color='white' />,
    cName: 'flex items-center px-5 py-2 text-base font-normal text-gray-900 rounded-lg ',
    click: '/logout',

  }
];